FROM ubuntu:20.04
LABEL maintainer="help@megabyte.space"

ENV container docker
ENV DEBIAN_FRONTEND noninteractive

# Source: https://github.com/geerlingguy/docker-ubuntu2004-ansible/blob/master/Dockerfile
# Source: https://github.com/j8r/dockerfiles/blob/master/systemd/ubuntu/20.04.Dockerfile
# Source: https://github.com/ansible/molecule/issues/1104

WORKDIR /

COPY initctl .

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3003,SC2010
RUN set -ex \
  && apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends \
      python3=3.* \
      python3-apt=2.* \
      sudo=1.* \
      systemd=* \
      systemd-cron=* \
      systemd-sysv=* \
  && apt-get clean \
  && rm -Rf /usr/share/doc \
      /usr/share/man \
      /tmp/* \
      /var/tmp/* \
  && chmod +x initctl \
  && rm -rf /sbin/initctl \
  && ln -s /initctl /sbin/initctl \
  && mkdir -p /etc/ansible \
  && echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts \
  && groupadd -r ansible \
  && useradd -m ansible -p ansible -g ansible \
  && usermod -aG sudo ansible \
  && sed -i "/^%sudo/s/ALL\$/NOPASSWD:ALL/g" /etc/sudoers \
  && cd /lib/systemd/system/sysinit.target.wants/ \
  && ls | grep -v systemd-tmpfiles-setup | xargs rm -f "$1" \
  && rm -f /lib/systemd/system/multi-user.target.wants/* \
      /etc/systemd/system/*.wants/* \
      /lib/systemd/system/local-fs.target.wants/* \
      /lib/systemd/system/sockets.target.wants/*udev* \
      /lib/systemd/system/sockets.target.wants/*initctl* \
      /lib/systemd/system/basic.target.wants/* \
      /lib/systemd/system/anaconda.target.wants/* \
      /lib/systemd/system/plymouth* \
      /lib/systemd/system/systemd-update-utmp* \
      /lib/systemd/system/systemd*udev* \
      /lib/systemd/system/getty.target

VOLUME ["/sys/fs/cgroup", "/tmp", "/run"]

CMD ["/lib/systemd/systemd"]
